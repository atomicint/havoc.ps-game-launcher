module net.soulsplit.launcher {
    exports net.soulsplit.launcher;

    requires jdk.incubator.httpclient;

    requires java.desktop;
    requires java.logging;

    requires gson;
    requires com.google.common;
}