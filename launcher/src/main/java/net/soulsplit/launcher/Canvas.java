package net.soulsplit.launcher;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;

/**
 * This Canvas class is responsible for our built-in launcher GUI.
 */
final class Canvas extends java.awt.Canvas {
    private static final String LAUNCHING = "Launching Havoc - please wait...";
    private static final Font HELVETICA_PLAIN = new Font("Helvetica", Font.PLAIN, 13);
    private static final Font HELVETICA_BOLD = new Font("Helvetica", Font.BOLD, 13);

    /**
     * The {@code Launcher} we represent.
     */
    private final Launcher launcher;

    /**
     * Construct a new Canvas relative to a {@code Launcher}.
     *
     * @param launcher the {@code Launcher} to represent.
     */
    Canvas(Launcher launcher) {
        this.launcher = launcher;
        setPreferredSize(new Dimension(350, 100));
        setSize(getPreferredSize());
    }

    /**
     * Re-renders the UI.
     */
    void render() {
        var strategy = getBufferStrategy();

        if (strategy == null) {
            createBufferStrategy(3);
            return;
        }

        var graphics = strategy.getDrawGraphics();
        var progress = launcher.getProgress();
        var text = launcher.getText();
        var width = getWidth();
        var height = getHeight();

        text += " - " + progress + "%";

        // this is to avoid flicker
        graphics.setColor(Color.BLACK);
        graphics.fillRect(0, 0, width, height);

        FontMetrics bold = getFontMetrics(HELVETICA_BOLD);
        FontMetrics plain = getFontMetrics(HELVETICA_PLAIN);

        var x = width >> 1;
        var y = (height >> 1) - 18;

        graphics.setColor(Color.RED);
        graphics.drawRect(x - 152, y, 304, 34);
        graphics.fillRect(x - 150, y + 2, progress * 3, 31);

        graphics.setColor(Color.BLACK);
        graphics.fillRect(x - 150 + progress * 3, y + 2, 300 - progress * 3, 31);

        graphics.setFont(HELVETICA_BOLD);
        graphics.setColor(Color.WHITE);
        graphics.drawString(text, width - bold.stringWidth(text) >> 1, y + 23);

        graphics.setFont(HELVETICA_PLAIN);
        graphics.drawString(LAUNCHING, width - plain.stringWidth(LAUNCHING) >> 1, y - 8);

        graphics.dispose();
        strategy.show();
    }

}
