package net.soulsplit.launcher;

import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

final class Configuration {

    @SerializedName("gamepack_url")
    private final String gamepackUrl;

    @SerializedName("gamepack_version")
    private final int gamepackVersion;

    @SerializedName("gamepack_params")
    private final String[] gamepackParameters;

    @SerializedName("gamepack_checksum")
    private final String gamepackChecksum;

    Configuration(String gamepackUrl, int gamepackVersion, String[] gamepackParameters, String gamepackChecksum) {
        this.gamepackUrl = Objects.requireNonNull(gamepackUrl);
        this.gamepackVersion = gamepackVersion;
        this.gamepackParameters = Objects.requireNonNull(gamepackParameters);
        this.gamepackChecksum = Objects.requireNonNull(gamepackChecksum);
    }

    int getGamepackVersion() {
        return gamepackVersion;
    }

    String getGamepackUrl() {
        return gamepackUrl;
    }

    String[] getGamepackParameters() {
        return gamepackParameters.clone();
    }

    String getGamepackChecksum() {
        return gamepackChecksum;
    }

    private static final class Builder {
        private String gamepackUrl;
        private int gamepackVersion;
        private String[] gamepackParameters;
        private String gamepackChecksum;

        public Builder setGamepackUrl(String gamepackUrl) {
            this.gamepackUrl = gamepackUrl;
            return this;
        }

        public Builder setGamepackVersion(int gamepackVersion) {
            this.gamepackVersion = gamepackVersion;
            return this;
        }

        public Builder setGamepackParameters(String[] gamepackParameters) {
            this.gamepackParameters = gamepackParameters;
            return this;
        }

        public Builder setGamepackChecksum(String gamepackChecksum) {
            this.gamepackChecksum = gamepackChecksum;
            return this;
        }

        public Configuration build() {
            return new Configuration(gamepackUrl, gamepackVersion, gamepackParameters, gamepackChecksum);
        }
    }

    public static final class TypeAdapter extends com.google.gson.TypeAdapter<Configuration> {
        @Override
        public void write(JsonWriter jsonWriter, Configuration configuration) {
            throw new UnsupportedOperationException("Writing of Configuration is unsupported.");
        }

        @Override
        public Configuration read(JsonReader jsonReader) throws IOException {
            Configuration.Builder builder = new Configuration.Builder();

            jsonReader.beginObject();
            while (jsonReader.hasNext()) {
                String name = jsonReader.nextName();

                if ("gamepack_url".equals(name)) {
                    builder.setGamepackUrl(jsonReader.nextString());
                } else if ("gamepack_version".equals(name)) {
                    builder.setGamepackVersion(jsonReader.nextInt());
                } else if ("gamepack_checksum".equals(name)) {
                    builder.setGamepackChecksum(jsonReader.nextString());
                } else if ("gamepack_params".equals(name)) {
                    List<String> params = new ArrayList<>();

                    jsonReader.beginArray();
                    while (jsonReader.hasNext()) {
                        params.add(jsonReader.nextString());
                    }
                    jsonReader.endArray();

                    builder.setGamepackParameters(params.toArray(new String[0]));
                }
            }
            jsonReader.endObject();

            return builder.build();
        }
    }

}
