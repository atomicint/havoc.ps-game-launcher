package net.soulsplit.launcher;

import java.nio.file.Path;

/**
 * This Constants class contains application constants.
 */
final class Constants {

    /**
     * The name of the application window.
     */
    static final String APPLICATION_NAME = "Havoc launcher";

    /**
     * The default connection timeout value to use with an {@code HttpClient}.
     */
    static final int CONNECTION_TIMEOUT = 30;

    /**
     * The URL from which we retrieve the launcher configurations.
     */
    static final String CONFIGURATION_URL = "http://assets.havoc.ps/config.json";

    /**
     * The Path of the local configuration.
     */
    static final Path LOCAL_CONFIGURATION = Environment.DATA_PATH.resolve("config.dat");

    /**
     * The Path of the {@code gamepack} Java archive.
     */
    static final Path GAMEPACK_FILE_PATH = Environment.DATA_PATH.resolve("gamepack.jar");

}
