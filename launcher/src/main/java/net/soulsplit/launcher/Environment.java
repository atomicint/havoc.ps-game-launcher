package net.soulsplit.launcher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * A static-utility class used to create and locate the workspace environment for all future sessions.
 */
final class Environment {

    /**
     * The root directory.
     */
    private static final Path ROOT = Paths.get(System.getProperty("user.home"), ".havoc");

    /**
     * The data Path.
     */
    static final Path DATA_PATH = ROOT.resolve("data");

    /**
     * Initializes this Environment's {@link #DATA_PATH}
     *
     * @throws IOException If some I/O exception occurs.
     */
    static void init() throws IOException {
        if (Files.notExists(DATA_PATH)) {
            Files.createDirectories(DATA_PATH);
        }
    }

}
