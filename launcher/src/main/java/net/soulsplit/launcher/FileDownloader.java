package net.soulsplit.launcher;

import com.google.common.hash.Hashing;
import com.google.common.io.MoreFiles;

import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This FileDownloader class is responsible for downloading the gamepack from the web.
 */
final class FileDownloader implements Runnable {

    /**
     * The Logger for this class.
     */
    private static final Logger logger = Logger.getLogger(FileDownloader.class.getSimpleName());

    /**
     * A reference to the {@code Launcher} that is awaiting our input.
     */
    private final Launcher launcher;

    /**
     * The configuration for this {@code Launcher}.
     */
    private final Configuration configuration;

    /**
     * Constructs a new {@link FileDownloader}.
     *
     * @param launcher      The Launcher that is awaiting our input.
     * @param configuration The configuration for this {@code Launcher}.
     */
    FileDownloader(Launcher launcher, Configuration configuration) {
        this.launcher = launcher;
        this.configuration = configuration;
    }

    @Override
    public void run() {
        // If an exception occurs we try again up to 10 times.
        var attemptCount = 0;

        while (attemptCount++ < Launcher.MAXIMUM_ATTEMPTS) {
            if (attemptCount > 1) {
                launcher.setLoadingInformation(0, "Attempting to establish connection (" + attemptCount + "/" + Launcher.MAXIMUM_ATTEMPTS + ")");
            } else {
                launcher.setLoadingInformation(0, "Attempting to establish connection");
            }

            try {
                var localVersion = LocalVersion.get(Constants.LOCAL_CONFIGURATION);

                if (configuration.getGamepackVersion() == localVersion.getId() && Files.exists(Constants.GAMEPACK_FILE_PATH)) {
                    var digest = MoreFiles.asByteSource(Constants.GAMEPACK_FILE_PATH).hash(Hashing.sha256());

                    if (digest.toString().equals(configuration.getGamepackChecksum())) {
                        launcher.setLoadingInformation(100, "Cached gamepack is OK");
                        launcher.finish(true, configuration);
                        return;
                    }
                }

                launcher.setLoadingInformation(0, "Updating gamepack...");
                IoUtil.bodyAsFile(configuration.getGamepackUrl(), Constants.GAMEPACK_FILE_PATH);

                var digest = MoreFiles.asByteSource(Constants.GAMEPACK_FILE_PATH).hash(Hashing.sha256());
                if (digest.toString().equals(configuration.getGamepackChecksum())) {
                    launcher.setLoadingInformation(100, "Successfully downloaded gamepack");
                    launcher.finish(true, configuration);
                    return;
                } else {
                    launcher.setLoadingInformation(0, "Checksum mismatched. Retrying...");

                    try {
                        Thread.sleep(1_000);
                    } catch (InterruptedException ignored) {
                    }
                }

            } catch (Exception cause) {
                logger.log(Level.SEVERE, "Exception occurred during launch", cause);
                launcher.setLoadingInformation(0, "Failed - retrying...");

                try {
                    Thread.sleep(1_000);
                } catch (InterruptedException ignored) {
                }
            }
        }
    }

}
