package net.soulsplit.launcher;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpClient.Redirect;
import jdk.incubator.http.HttpRequest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.time.Duration;

import static java.time.temporal.ChronoUnit.SECONDS;
import static jdk.incubator.http.HttpResponse.BodyHandler;

/**
 * A static utility class for I/O
 */
final class IoUtil {
    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";

    static HttpRequest cachelessHttpRequest(String url) {
        try {
            return HttpRequest.newBuilder()
                    .uri(new URI(url))
                    .setHeader("User-Agent", USER_AGENT)
                    .setHeader("Cache-Control", "no-store,max-age=0,no-cache")
                    .setHeader("Expires", "0")
                    .setHeader("Pragma", "no-cache")
                    .timeout(Duration.of(Constants.CONNECTION_TIMEOUT, SECONDS))
                    .GET()
                    .build();
        } catch (URISyntaxException cause) {
            throw new UnsupportedOperationException("Unsupported URI: " + url, cause);
        }
    }

    static HttpClient neverRedirectHttpClient() {
        return HttpClient.newBuilder()
                .followRedirects(Redirect.NEVER)
                .build();
    }

    static void bodyAsFile(String url, Path localFile) throws IOException, InterruptedException {
        var request = cachelessHttpRequest(url);
        neverRedirectHttpClient().send(request, BodyHandler.asFile(localFile));
    }

    static String bodyAsString(String url) throws IOException, InterruptedException {
        var request = cachelessHttpRequest(url);
        var response = neverRedirectHttpClient().send(request, BodyHandler.asString());
        return response.body();
    }

}
