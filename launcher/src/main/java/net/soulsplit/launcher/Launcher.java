package net.soulsplit.launcher;

import com.google.gson.GsonBuilder;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashSet;

/**
 * This is the application entry-point and is responsible for the flow.
 */
public final class Launcher extends JFrame implements Runnable {
    static final int MAXIMUM_ATTEMPTS = 10;

    /**
     * Generated serial version UID to avoid IDE warnings.
     */
    private static final long serialVersionUID = -7928661533827082944L;

    /**
     * An indicator on whether to continue rendering onto the UI or not.
     */
    private boolean active;

    /**
     * The canvas responsible for our UI content.
     */
    private Canvas canvas;

    /**
     * A {@code FileDownloader} currently running to download/verify the gamepack.
     */
    private FileDownloader downloader;

    /**
     * The thread responsible for downloading the gamepack from the web server.
     */
    private Thread downloaderThread;

    /**
     * The current progress percent.
     */
    private int progress = 0;

    /**
     * Our current status. This is visible on the UI.
     */
    private String text = "";

    /**
     * The number of attempts.
     */
    private int attempts = 0;

    private Launcher() {
        super(Constants.APPLICATION_NAME);
        canvas = new Canvas(this);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        add(canvas);
        pack();
        setVisible(true);

        new Thread(() -> {
            while (canvas != null) {
                canvas.render();
            }
        }).start();
    }

    private void notifyStacktrace(Throwable cause) {
        var sw = new StringWriter();
        var pw = new PrintWriter(sw, true);
        cause.printStackTrace(pw);
        JOptionPane.showMessageDialog(this, sw.getBuffer().toString(), "Error", JOptionPane.ERROR_MESSAGE);
        this.finish(false, null);
    }

    public static void main(String[] args) {
        var launcher = new Launcher();

        try {
            Environment.init();
            launcher.run();
        } catch (Exception cause) {
            launcher.notifyStacktrace(cause);
        }
    }

    @Override
    public void run() {
        if (attempts > 0) {
            setLoadingInformation(0, "Fetching launcher configuration (" + attempts + "/" + MAXIMUM_ATTEMPTS + ")...");
        } else {
            setLoadingInformation(0, "Fetching launcher configuration...");
        }

        var gson = new GsonBuilder()
                .registerTypeAdapter(Configuration.class, new Configuration.TypeAdapter())
                .create();

        try {
            var json = IoUtil.bodyAsString(Constants.CONFIGURATION_URL);
            var configuration = gson.fromJson(json, Configuration.class);
            downloader = new FileDownloader(this, configuration);
        } catch (IOException | InterruptedException cause) {
            if (attempts++ >= MAXIMUM_ATTEMPTS) {
                throw new RuntimeException("Unable to read configuration at URL: " + Constants.CONFIGURATION_URL, cause);
            }
            // Retry...
            run();
            return;
        }

        setLoadingInformation(0, "Preparing to verify gamepack version.");

        if (downloader != null) {
            downloaderThread = new Thread(downloader);
            downloaderThread.start();
            active = true;
        }

        if (!active) {
            if (downloader == null) {
                throw new NullPointerException("Downloader not initialized - unable to start");
            }
            if (downloaderThread == null) {
                throw new NullPointerException("Downloader thread not initialized - unable to start");
            }
        }
    }

    synchronized void setLoadingInformation(int progress, String text) {
        this.progress = progress;
        this.text = text;
    }

    void finish(boolean success, Configuration configuration) {
        if (success) {
            var arguments = new LinkedHashSet<String>();

            try {
                var javaHomeLocation = System.getProperty("java.home");
                var baseJavaPath = Paths.get(javaHomeLocation);

                if (Files.notExists(baseJavaPath)) {
                    throw new IOException("Java path: " + baseJavaPath + " does not point to a valid Java installation.");
                }

                var path = baseJavaPath.resolve("bin").resolve("java");
                arguments.add(path.toString());
                arguments.addAll(Arrays.asList(configuration.getGamepackParameters()));
                arguments.add("-jar");
                arguments.add(Constants.GAMEPACK_FILE_PATH.toString());
                arguments.add("-launcher");

                var builder = new ProcessBuilder(arguments.toArray(new String[0]));
                builder.start();

                LocalVersion.put(Constants.LOCAL_CONFIGURATION, configuration.getGamepackVersion());
            } catch (IOException cause) {
                notifyStacktrace(cause);
            }
        }

        setVisible(false);
        downloaderThread.interrupt();
        active = false;
        canvas = null;
        downloader = null;
        downloaderThread = null;
        System.gc();
        Thread.currentThread().interrupt();
        System.exit(0);
    }

    synchronized int getProgress() {
        return progress;
    }

    synchronized String getText() {
        return text;
    }

}
