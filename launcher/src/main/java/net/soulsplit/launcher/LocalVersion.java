package net.soulsplit.launcher;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.StandardOpenOption.*;

/**
 * Represents the local version of this Launcher.
 * <p>
 * This version should be compared to the version found within {@link Configuration} to determine validity.
 * </p>
 */
public final class LocalVersion {

    /**
     * A {@link LocalVersion} instance which denotes there is no known local version.
     */
    private static final LocalVersion NO_VERSION = new LocalVersion(-1);

    /**
     * Creates or updates the LocalVersion for this Launcher.
     *
     * @param path The {@code Path} of the local version.
     * @param id   The new local version id.
     * @throws IOException If some I/O exception occurs.
     */
    static void put(Path path, int id) throws IOException {
        var buffer = ByteBuffer.allocate(Integer.BYTES);
        buffer.putInt(id);

        Files.write(path, buffer.array(), WRITE, CREATE, TRUNCATE_EXISTING);
    }

    /**
     * Gets an instance of {@link LocalVersion} for the specified {@code Path}.
     *
     * @param path The {@code Path} of the local version.
     * @return The LocalVersion, if it exists. Otherwise {@link LocalVersion#NO_VERSION} is returned.
     * @throws IOException If some I/O exception occurs.
     */
    static LocalVersion get(Path path) throws IOException {
        if (Files.notExists(path)) {
            return NO_VERSION;
        }

        var buffer = ByteBuffer.wrap(Files.readAllBytes(path));
        return new LocalVersion(buffer.getInt());
    }

    /**
     * The id of this version.
     */
    private final int id;

    /**
     * Constructs a new {@link LocalVersion}.
     *
     * @param id The id of this version.
     */
    LocalVersion(int id) {
        this.id = id;
    }

    /**
     * Gets the id of this version.
     *
     * @return The id of this version.
     */
    int getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof LocalVersion) {
            var other = (LocalVersion) obj;
            return id == other.getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return id;
    }

}
